#include <stdlib.h>
#include <assert.h>
#include <stdio.h>

#include "mmlogic.h"

int main() {
    printf("Checking spaces == 4...\n");
    {
        unsigned int guess[5] =  {1, 2, 3, 4, 0};
        unsigned int secret[5] = {1, 2, 3, 4, 0};
        unsigned int expected = 40;
        unsigned int result = check_guess(guess, 4, secret);
        if (result != expected) {
            printf("FAIL: check_guess({1, 2, 3, 4, 0}, {1, 2, 3, 4, 0}) returned %d; expected %d\n", result, expected);
            assert(result == expected);
        }
    }

    {
        unsigned int guess[5] =  {1, 2, 3, 4, 0};
        unsigned int secret[5] = {1, 3, 2, 4, 0};
        unsigned int expected = 22;
        unsigned int result = check_guess(guess, 4, secret);
        if (result != expected) {
            printf("FAIL: check_guess({1, 2, 3, 4, 0}, {1, 3, 2, 4, 0}) returned %d; expected %d\n", result, expected);
            assert(result == expected);
        }
    }

    {
        unsigned int guess[5] =  {1, 2, 3, 4, 0};
        unsigned int secret[5] = {3, 4, 2, 4, 0};
        unsigned int expected = 12;
        unsigned int result = check_guess(guess, 4, secret);
        if (result != expected) {
            printf("FAIL: check_guess({1, 2, 3, 4, 0}, {3, 4, 2, 4, 0}) returned %d; expected %d\n", result, expected);
            assert(result == expected);
        }
    }

    {
        unsigned int guess[5] =  {4, 4, 4, 4, 0};
        unsigned int secret[5] = {1, 4, 2, 2, 0};
        unsigned int expected = 10;
        unsigned int result = check_guess(guess, 4, secret);
        if (result != expected) {
            printf("FAIL: check_guess({4, 4, 4, 4, 0}, {1, 4, 2, 2, 0}) returned %d; expected %d\n", result, expected);
            assert(result == expected);
        }
    }

    {
        unsigned int guess[5] =  {3, 2, 3, 2, 0};
        unsigned int secret[5] = {2, 2, 3, 3, 0};
        unsigned int expected = 22;
        unsigned int result = check_guess(guess, 4, secret);
        if (result != expected) {
            printf("FAIL: check_guess({3, 2, 3, 2, 0}, {2, 2, 3, 3, 0}) returned %d; expected %d\n", result, expected);
            assert(result == expected);
        }
    }

    {
        unsigned int guess[5] =  {2, 3, 3, 2, 0};
        unsigned int secret[5] = {3, 3, 3, 3, 0};
        unsigned int expected = 20;
        unsigned int result = check_guess(guess, 4, secret);
        if (result != expected) {
            printf("FAIL: check_guess({2, 3, 3, 2, 0}, {3, 3, 3, 3, 0}) returned %d; expected %d\n", result, expected);
            assert(result == expected);
        }
    }

    {
        unsigned int guess[5] =  {1, 3, 2, 2, 0};
        unsigned int secret[5] = {2, 2, 3, 3, 0};
        unsigned int expected = 3;
        unsigned int result = check_guess(guess, 4, secret);
        if (result != expected) {
            printf("FAIL: check_guess({1, 3, 2, 2, 0}, {2, 2, 3, 3, 0}) returned %d; expected %d\n", result, expected);
            assert(result == expected);
        }
    }

    {
        unsigned int guess[5] =  {0, 0, 0, 0, 0};
        unsigned int secret[5] = {0, 0, 0, 1, 0};
        unsigned int expected = 30;
        unsigned int result = check_guess(guess, 4, secret);
        if (result != expected) {
            printf("FAIL: check_guess({1, 3, 2, 2, 0}, {2, 2, 3, 3, 0}) returned %d; expected %d\n", result, expected);
            assert(result == expected);
        }
    }

    {
        unsigned int guess[5] =  {4, 2, 3, 5, 0};
        unsigned int secret[5] = {5, 5, 4, 2, 0};
        unsigned int expected = 3;
        unsigned int result = check_guess(guess, 4, secret);
        if (result != expected) {
            printf("FAIL: check_guess({4, 2, 3, 5, 0}, {5, 5, 4, 2, 0}) returned %d; expected %d\n", result, expected);
            assert(result == expected);
        }
    }

    {
        unsigned int guess[5] =  {5, 5, 4, 2, 0};
        unsigned int secret[5] = {4, 2, 3, 5, 0};
        unsigned int expected = 3;
        unsigned int result = check_guess(guess, 4, secret);
        if (result != expected) {
            printf("FAIL: check_guess({5, 5, 4, 2, 0}, {4, 2, 3, 5, 0}) returned %d; expected %d\n", result, expected);
            assert(result == expected);
        }
    }

    {
        unsigned int guess[5] =  {7, 1, 12, 6, 0};
        unsigned int secret[5] = {3, 9, 12, 7, 0};
        unsigned int expected = 11;
        unsigned int result = check_guess(guess, 4, secret);
        if (result != expected) {
            printf("FAIL: check_guess({7, 1, 12, 6, 0}, {3, 9, 12, 7, 0}) returned %d; expected %d\n", result, expected);
            assert(result == expected);
        }
    }

    printf("Checking spaces == 5...\n");
    {
        unsigned int guess[5] =  {1, 2, 3, 4, 0};
        unsigned int secret[5] = {1, 2, 3, 4, 0};
        unsigned int expected = 50;
        unsigned int result = check_guess(guess, 5, secret);
        if (result != expected) {
            printf("FAIL: check_guess({1, 2, 3, 4, 0}, {1, 2, 3, 4, 0}) returned %d; expected %d\n", result, expected);
            assert(result == expected);
        }
    }

    {
        unsigned int guess[5] =  {1, 2, 3, 4, 0};
        unsigned int secret[5] = {1, 3, 2, 4, 0};
        unsigned int expected = 32;
        unsigned int result = check_guess(guess, 5, secret);
        if (result != expected) {
            printf("FAIL: check_guess({1, 2, 3, 4, 0}, {1, 3, 2, 4, 0}) returned %d; expected %d\n", result, expected);
            assert(result == expected);
        }
    }

    {
        unsigned int guess[5] =  {1, 2, 3, 4, 0};
        unsigned int secret[5] = {3, 4, 2, 4, 0};
        unsigned int expected = 22;
        unsigned int result = check_guess(guess, 5, secret);
        if (result != expected) {
            printf("FAIL: check_guess({1, 2, 3, 4, 0}, {3, 4, 2, 4, 0}) returned %d; expected %d\n", result, expected);
            assert(result == expected);
        }
    }

    {
        unsigned int guess[5] =  {4, 4, 4, 4, 0};
        unsigned int secret[5] = {1, 4, 2, 2, 0};
        unsigned int expected = 20;
        unsigned int result = check_guess(guess, 5, secret);
        if (result != expected) {
            printf("FAIL: check_guess({4, 4, 4, 4, 0}, {1, 4, 2, 2, 0}) returned %d; expected %d\n", result, expected);
            assert(result == expected);
        }
    }

    {
        unsigned int guess[5] =  {3, 2, 3, 2, 0};
        unsigned int secret[5] = {2, 2, 3, 3, 0};
        unsigned int expected = 32;
        unsigned int result = check_guess(guess, 5, secret);
        if (result != expected) {
            printf("FAIL: check_guess({3, 2, 3, 2, 0}, {2, 2, 3, 3, 0}) returned %d; expected %d\n", result, expected);
            assert(result == expected);
        }
    }

    {
        unsigned int guess[5] =  {2, 3, 3, 2, 0};
        unsigned int secret[5] = {3, 3, 3, 3, 0};
        unsigned int expected = 30;
        unsigned int result = check_guess(guess, 5, secret);
        if (result != expected) {
            printf("FAIL: check_guess({2, 3, 3, 2, 0}, {3, 3, 3, 3, 0}) returned %d; expected %d\n", result, expected);
            assert(result == expected);
        }
    }

    {
        unsigned int guess[5] =  {1, 3, 2, 2, 0};
        unsigned int secret[5] = {2, 2, 3, 3, 0};
        unsigned int expected = 13;
        unsigned int result = check_guess(guess, 5, secret);
        if (result != expected) {
            printf("FAIL: check_guess({1, 3, 2, 2, 0}, {2, 2, 3, 3, 0}) returned %d; expected %d\n", result, expected);
            assert(result == expected);
        }
    }

    {
        unsigned int guess[5] =  {0, 0, 0, 0, 0};
        unsigned int secret[5] = {0, 0, 0, 1, 0};
        unsigned int expected = 40;
        unsigned int result = check_guess(guess, 5, secret);
        if (result != expected) {
            printf("FAIL: check_guess({1, 3, 2, 2, 0}, {2, 2, 3, 3, 0}) returned %d; expected %d\n", result, expected);
            assert(result == expected);
        }
    }

    {
        unsigned int guess[5] =  {4, 2, 3, 5, 0};
        unsigned int secret[5] = {5, 5, 4, 2, 0};
        unsigned int expected = 13;
        unsigned int result = check_guess(guess, 5, secret);
        if (result != expected) {
            printf("FAIL: check_guess({4, 2, 3, 5, 0}, {5, 5, 4, 2, 0}) returned %d; expected %d\n", result, expected);
            assert(result == expected);
        }
    }

    {
        unsigned int guess[5] =  {5, 5, 4, 2, 0};
        unsigned int secret[5] = {4, 2, 3, 5, 0};
        unsigned int expected = 13;
        unsigned int result = check_guess(guess, 5, secret);
        if (result != expected) {
            printf("FAIL: check_guess({5, 5, 4, 2, 0}, {4, 2, 3, 5, 0}) returned %d; expected %d\n", result, expected);
            assert(result == expected);
        }
    }

    {
        unsigned int guess[5] =  {7, 1, 12, 6, 0};
        unsigned int secret[5] = {3, 9, 12, 7, 0};
        unsigned int expected = 21;
        unsigned int result = check_guess(guess, 5, secret);
        if (result != expected) {
            printf("FAIL: check_guess({7, 1, 12, 6, 0}, {3, 9, 12, 7, 0}) returned %d; expected %d\n", result, expected);
            assert(result == expected);
        }
    }

    return EXIT_SUCCESS;
}

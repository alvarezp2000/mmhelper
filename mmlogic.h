/*
 * mmhelper/mmlogic.h - Mastermind helper, logic routines headers
 * Copyright (C) 2020  Octavio Alvarez Piza
 * SPDX-License-Identifier: AGPL-3.0-only
 *
 * This program is distributed under the Affero GPL 3.0 license.
 * Licensing terms are included in the LICENSE file.
 * 
 * This program is distributed WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef __MMLOGIC_H
#define __MMLOGIC_H

typedef unsigned int code[5];

enum code_selection {
    POSSIBLE = 0,
    DISCARDED = 1
};

unsigned int check_guess(code const secret, unsigned int spaces, code const code);

void eliminate_options(
    // Input
    enum code_selection option_set[8][8][8][8][8],
    unsigned int spaces,
    code const guess,
    unsigned int const result,
    // Output
    enum code_selection result_option_set[8][8][8][8][8]
);

#endif /* __MMLOGIC_H */
